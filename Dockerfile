FROM python:3.12.0-bookworm

WORKDIR /usr/src/app

COPY requirements.txt .

RUN apt-get update
RUN apt-get -y install build-essential libchm-dev libpq-dev
RUN pip install -r requirements.txt

COPY helparchive/ helparchive/
COPY manage.py manage.py
COPY viewer/ viewer/

CMD ["python3", "manage.py", "runserver"]
