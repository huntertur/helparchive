# helparchive: A web app for managing a Microsoft KnowledgeBase archive

Copyright (C) 2022-2023 Hunter Turcin

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Demo

![helparchive listing all articles under the category Windows 3.1 and Windows for Workgroups](demo.png)

See helparchive in action at: https://helparchive.huntertur.net

## Why make another archive?

There is another, more popular KnowledgeBase article archive in existence.
This other archive is fantastic but incomplete. I had attempted to make contact
with the maintainer of this archive to add the articles I had in my collection,
but I unfortunately was not successful. Additionally, this other archive is
mainly geared toward serving plain text versions of articles, whereas I had
HTML versions in part of my collection.

## Setup

```sql
CREATE DATABASE helparchive;
CREATE USER helparchive WITH ENCRYPTED PASSWORD 'something secret';
GRANT ALL PRIVILEGES ON DATABASE helparchive TO helparchive;
```

```sh
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
mkdir instance
echo 'something random and long' > instance/secret_key.txt
echo 'something secret' > instance/database_password.txt
echo 'False' > instance/debug.txt
echo 'localhost' > instance/database_host.txt
echo '8000' > instance/port.txt
echo 'http://127.0.0.1:8000' > instance/origin.txt
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver
```

Then, head to `http://127.0.0.1/admin` to login as the superuser account,
then head to `http://127.0.0.1/upload/chm` to upload articles from
a `KB.CHM` file, or `http://127.0.0.1/upload/txt_zip` to upload articles
from a compressed archive of text files.

Please see the Help page inside the application for further information.
