from django.db import models

# Create your models here.

class Author(models.Model):
    """Represents the original author of a document"""
    name = models.TextField()

    def __str__(self):
        return self.name

class Category(models.Model):
    """Represents the author's category for the document"""
    name = models.TextField()

    def __str__(self):
        return self.name

class ContentType(models.Model):
    """Represents a MIME type, like text/html"""
    mime_type = models.TextField()

    def __str__(self):
        return self.mime_type

class Source(models.Model):
    """Represents the source of a variant"""
    name = models.TextField()

    def __str__(self):
        return self.name

class Document(models.Model):
    """Represents a document with possibly multiple variants"""
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    categories = models.ManyToManyField(Category)
    original_id = models.TextField(null=True)
    title = models.TextField()

    def __str__(self):
        if self.original_id:
            return f'{self.original_id}: {self.title}'
        else:
            return self.title

    def get_newest_variant(self) -> 'DocumentVariant':
        variant = (
            DocumentVariant.objects
            .filter(document__id=self.id)
            .exclude(visibility=Visibility.HIDDEN)
            .order_by('revised_at')
            .last()
        )

        return variant

class Visibility(models.IntegerChoices):
    """Represents if a revision of a document has been human-checked"""
    UNREVIEWED = 1
    REVIEWED = 2
    HIDDEN = 3

class DocumentVariant(models.Model):
    """Represents a revision of a document"""
    content = models.TextField()
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    document = models.ForeignKey(Document, on_delete=models.CASCADE)
    revised_at = models.DateField()
    source = models.ForeignKey(Source, on_delete=models.CASCADE, null=True)
    visibility = models.IntegerField(choices=Visibility.choices,
                                     default=Visibility.UNREVIEWED)

    def __str__(self):
        return self.content
