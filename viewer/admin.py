from django.contrib import admin

from .models import (Author, Category, ContentType, Document, DocumentVariant,
                     Source)

# Register your models here.

admin.site.register((
    Author, Category, ContentType, Document, DocumentVariant, Source
))
