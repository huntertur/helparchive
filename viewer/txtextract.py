from datetime import datetime
import logging
from tempfile import NamedTemporaryFile
from zipfile import Path, ZipFile

from .models import (Author, Category, ContentType, Document, DocumentVariant,
                     Source)

LOGGER = logging.getLogger('django')

real_author, _ = Author.objects.get_or_create(name='Microsoft Corporation')
real_type, _ = ContentType.objects.get_or_create(mime_type='text/plain')

def process_entry(content: str, source: str):
    # This expects the Microsoft FTP KB format to be used
    lines = content.splitlines()
    if not lines[0].startswith('DOCUMENT:'):
        LOGGER.warning('Invalid KB document: %s', lines[0])
        return
    category = lines[2][9:]
    original_id = lines[0][9:16].strip()
    revised_at = datetime.strptime(lines[0][17:29].strip(), '%d-%b-%Y')
    title = lines[1][9:]

    real_category, _ = Category.objects.get_or_create(name=category)
    document, _ = Document.objects.get_or_create(author=real_author,
                                                 original_id=original_id,
                                                 title=title)
    document.categories.add(real_category)
    real_source, _ = Source.objects.get_or_create(name=source)
    variant, _ = DocumentVariant.objects.get_or_create(content='\n'.join(lines),
                                                       content_type=real_type,
                                                       document=document,
                                                       revised_at=revised_at,
                                                       source=real_source)

def process(content: bytes, source: str):
    with NamedTemporaryFile() as tmp:
        tmp.write(content)
        tmp.seek(0)
        with ZipFile(tmp.name) as archive:
            for name in archive.namelist():
                if not name.endswith('.TXT'):
                    LOGGER.info('Skipping file: %s', name)
                    continue
                path = Path(archive, name)
                process_entry(path.read_text(encoding='iso-8859-1'), source)
