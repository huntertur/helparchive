from django.urls import path

from . import views

urlpatterns = [
    path('', views.view_categories, name='index'),
    path('categories', views.view_categories, name='view_categories'),
    path('category/<int:category_id>', views.view_category,
         name='view_category'),
    path('check/small', views.check_small, name='check_small'),
    path('document/<int:document_id>', views.view_best, name='view_best'),
    path('document/', views.view_best_original_id,
         name='view_best_original_id'),
    path('help', views.help, name='help'),
    path('login', views.login, name='login'),
    path('merge', views.merge, name='merge'),
    path('search', views.search, name='search'),
    path('statistics', views.statistics, name='statistics'),
    path('upload/chm', views.upload_chm, name='upload_chm'),
    path('upload/txt_zip', views.upload_txt_zip, name='upload_txt_zip'),
    path('variant/<int:variant_id>', views.view_variant,
         name='view_variant'),
    path('variants/', views.view_variants_original_id,
         name='view_variants_original_id'),
]
