"""
This is very loosely based on msdn98extract:
https://gitlab.com/huntertur/msdn98extract
"""
from datetime import datetime
import logging
import re
from tempfile import NamedTemporaryFile

from bs4 import BeautifulSoup, Tag
from chm.chm import CHMCollection

from .models import (Author, Category, ContentType, Document, DocumentVariant,
                     Source)

LOGGER = logging.getLogger('django')

ARTICLE_ID_BODY = re.compile(r'<B>ID: (Q\d+)</B>')
ARTICLE_ID_PREFIX = re.compile(r'^Q\d+ - ')
LAST_REVIEWED_MATCH = re.compile(r'<P>Last Reviewed: ([^<]+)</P>')

real_author, _ = Author.objects.get_or_create(name='Microsoft Corporation')
real_type, _ = ContentType.objects.get_or_create(mime_type='text/html')

def process_child(html: str, source: str, type: str):
    soup = BeautifulSoup(html, 'lxml')

    try:
        original_id = soup.find('meta', {'name': 'KBID'}).get('content').strip()
    except AttributeError:
        # Q192668 has no KBID
        original_id = ARTICLE_ID_BODY.search(html).group(1)
        LOGGER.warning('No KBID for current document; found %s', original_id)

    try:
        category = soup.find('meta', {'name': 'Product'}).get('content')
    except AttributeError:
        # Q164764 has no Product
        LOGGER.warning('No category for: %s', original_id)
        category = '(Uncategorized)'

    try:
        revised_at_raw = soup.find('meta', {'name': 'KBModify'}).get('content')
    except AttributeError:
        # also Q164764
        LOGGER.warning('Working around missing KBModify for: %s', original_id)
        revised_at_raw = LAST_REVIEWED_MATCH.search(html).group(1)

    try:
        # Everything in MSDN Library 1998 is in this format
        revised_at = datetime.strptime(revised_at_raw, '%Y/%m/%d')
    except ValueError:
        # Almost everything in 1999 is in this format, but some is 1998 format
        revised_at = datetime.strptime(revised_at_raw, '%B %d, %Y')

    title = soup.title.text.strip()
    if type == '1999':
        title = ARTICLE_ID_PREFIX.sub('', title)

    real_category, _ = Category.objects.get_or_create(name=category)
    document, _ = Document.objects.get_or_create(author=real_author,
                                                 original_id=original_id,
                                                 title=title)
    document.categories.add(real_category)
    real_source, _ = Source.objects.get_or_create(name=source)
    variant, _ = DocumentVariant.objects.get_or_create(content=html,
                                                       content_type=real_type,
                                                       document=document,
                                                       revised_at=revised_at,
                                                       source=real_source)

def process_topic(chm: CHMCollection, tag: Tag, source: str, type: str):
    # Ignore whitespace
    if not isinstance(tag, Tag):
        return

    # Ignore "About" topics
    if len(tag.contents) < 4:
        return

    # 0: whitespace
    # 1: parent information
    # 2: whitespace
    # 3: child tags
    # 4: whitespace
    # For most of 1999, the initial whitespace is not present
    if len(tag.contents) < 4:
        start = tag.contents[2]
    else:
        start = tag.contents[3]
        if not isinstance(start, Tag):
            start = tag.contents[2]
    for child in start.find_all('li'):
        if not isinstance(child, Tag):
            continue

        child: Tag

        obj: Tag = child.object
        found = obj.find('param', {'name': 'Local'})
        if not found:
            LOGGER.info('skipping child with no Local: %s', obj)
            continue
        local = found.get('value')

        success, info, file = chm.ResolveObject(f'/{local}'.encode())

        if success != 0:
            raise IOError(f'cannot resolve object: /{local}')

        child_html_encoded = file.RetrieveObject(info)[1]
        try:
            child_html = child_html_encoded.decode('cp1252')
        except UnicodeDecodeError:
            # At least one article (Q234748) needs this
            LOGGER.info('overriding to iso-8859-1 for: %s', local)
            child_html = child_html_encoded.decode('iso-8859-1')
        process_child(child_html, source, type)

def process(chm_buf: bytes, chi_buf: bytes | None, source: str, type: str):
    LOGGER.info('Loading CHM file')
    chm = CHMCollection()
    with NamedTemporaryFile() as chm_tmp, NamedTemporaryFile() as chi_tmp:
        chm_tmp.write(chm_buf)
        chm_tmp.seek(0)
        filenames = [chm_tmp.name]
        if chi_buf:
            chi_tmp.write(chi_buf)
            chi_tmp.seek(0)
            filenames.append(chi_tmp.name)
        if not chm.LoadFiles(*filenames):
            raise IOError(f'unable to load: {filenames}')
    html = chm.GetTopicsTree()
    LOGGER.info('Parsing topics tree')
    soup = BeautifulSoup(html, 'lxml')
    # Hardcode this to skip over the lonely "about" topic for 1998 version
    offset = 3 if type == '1998' else 1
    root: Tag = soup.html.body.contents[offset]
    for parent_tag in root.children:
        process_topic(chm, parent_tag, source, type)
