import logging

from django.contrib.auth.decorators import login_required
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.db.models.functions import Length
from django.http import Http404, HttpRequest, HttpResponse
from django.shortcuts import get_object_or_404, redirect, render

from . import chmextract
from .models import Category, Document, DocumentVariant, Visibility
from . import txtextract

logger = logging.getLogger(__name__)

# Create your views here.

def view_categories(request: HttpRequest):
    """View all categories"""
    # Do this instead of order_by to make (Uncategorized) be at the top
    categories = sorted(Category.objects.all(), key=lambda c: c.name.lower())
    context = {
        'categories': categories,
        'tab': 'categories',
    }
    return render(request, 'viewer/categories.html', context)

def view_category(request: HttpRequest, category_id: int):
    """View all articles for a category"""
    category = get_object_or_404(Category, id=category_id)
    order = request.GET.get('sort', None)
    if order not in ('original_id', 'title'):
        order = 'title'
    documents = Document.objects.filter(categories=category_id)
    if order == 'original_id':
        # Deal with identifiers like Q3, Q11
        documents = documents.order_by(Length('original_id'), 'original_id')
    else:
        documents = documents.order_by(order)
    context = {
        'category': category,
        'documents': documents,
        'tab': 'category',
    }
    return render(request, 'viewer/category.html', context)

def view_best(request: HttpRequest, document_id: int):
    """View the best variant of a single document"""
    try:
        document = Document.objects.get(id=document_id)
    except ObjectDoesNotExist:
        raise Http404('no document for this id')
    variant = document.get_newest_variant()
    response = HttpResponse(variant.content)
    response.headers['Content-Type'] = variant.content_type.mime_type
    return response

def view_best_original_id(request: HttpRequest):
    """View the best variant of a single document by its original ID"""
    if not (original_id := request.GET.get('kbid')):
        raise Http404('no original ID specified')
    try:
        document = Document.objects.get(original_id=original_id)
    except ObjectDoesNotExist:
        raise Http404('no document for this original ID')
    except MultipleObjectsReturned:
        logger.warning('Multiple articles have original ID %s', original_id)
        documents = Document.objects.filter(original_id=original_id)
        document = documents[0]
        for doc in documents:
            if (doc.get_newest_variant().revised_at
                > document.get_newest_variant().revised_at):
                document = doc
        logger.warning('Choosing id = %d', document.id)
    return redirect('view_best', document_id=document.id)

def view_variants_original_id(request: HttpRequest):
    """View all variants for a document by its original ID"""
    if not (original_id := request.GET.get('kbid')):
        raise Http404('no original ID specified')
    try:
        document = Document.objects.get(original_id=original_id)
    except ObjectDoesNotExist:
        raise Http404('no document for this original ID')
    variants = (
        DocumentVariant.objects
        .filter(document__id=document.id)
        .order_by('revised_at')
        .reverse()
    )
    context = {
        'document': document,
        'tab': 'results',
        'variants': variants,
    }
    return render(request, 'viewer/variants.html', context)

def view_variant(request: HttpRequest, variant_id: int):
    """View a specific variant of a document"""
    try:
        variant = DocumentVariant.objects.get(id=variant_id)
    except ObjectDoesNotExist:
        raise Http404('no variant for this ID')
    response = HttpResponse(variant.content)
    response.headers['Content-Type'] = variant.content_type.mime_type
    return response

def search(request: HttpRequest):
    """Search the archive"""
    context = {
        'tab': 'search',
    }
    return render(request, 'viewer/search.html', context)

def statistics(request: HttpRequest):
    """View statistics about the archive"""
    context = {
        'categories': Category.objects.count(),
        'documents': Document.objects.count(),
        'variants': DocumentVariant.objects.count(),
        'tab': 'statistics',
    }
    return render(request, 'viewer/statistics.html', context)

def help(request: HttpRequest):
    """View the help page"""
    return render(request, 'viewer/help.html', {'tab': 'help'})

def login(request: HttpRequest):
    """Try to login"""
    return render(request, 'viewer/login.html')

@login_required
def merge(request: HttpRequest):
    """Merge duplicated categories"""
    surviving_id = request.POST.get('surviving_id')
    redirected_id = request.POST.get('redirected_id')
    if not surviving_id and not redirected_id:
        message = 'Please select two categories to merge.'
    elif surviving_id == redirected_id:
        message = 'Categories cannot be the same.'
    else:
        surviving = Category.objects.get(id=surviving_id)
        redirected = Category.objects.get(id=redirected_id)
        docs = Document.objects.filter(categories__id=redirected_id)
        for doc in docs:
            if not doc.categories.contains(surviving):
                doc.categories.add(surviving)
            doc.categories.remove(redirected)
        redirected.delete()
        message = f'{surviving.name} has eaten {redirected.name}.'
    context = {
        'categories': Category.objects.all().order_by('name'),
        'message': message,
    }
    return render(request, 'viewer/merge.html', context)

@login_required
def upload_chm(request: HttpRequest):
    """Upload a KB.CHM from an MSDN CD and absorb its articles"""
    chm = request.FILES.get('chm')
    chi = request.FILES.get('chi')
    if chm:
        chmextract.process(chm.read(), chi.read() if chi else None,
                           request.POST['source'], request.POST['type'])
        message = f'{chm.name} '
        if chi:
            message += f'and {chi.name} '
        message += ' processed.'
    else:
        message = ('Please upload a CHM file and optionally its companion CHI '
                   'file for processing.')
    return render(request, 'viewer/upload_chm.html', {'message': message})

@login_required
def upload_txt_zip(request: HttpRequest):
    """Upload a zip of FTP KB articles absorb its articles"""
    if request.FILES.get('zip', None):
        txtextract.process(request.FILES['zip'].read(),
                           request.POST['source'])
        message = f'{request.FILES["zip"].name} processed.'
    else:
        message = 'Please upload a zip archive for processing.'
    return render(request, 'viewer/upload_txt_zip.html', {'message': message})

@login_required
def check_small(request: HttpRequest):
    """Check a list of suspiciously small document variants"""
    if request.method == 'POST':
        for key, value in request.POST.items():
            if not key.startswith('option-'):
                continue
            variant_id = int(key.split('-')[1])
            variant = DocumentVariant.objects.get(id=variant_id)
            variant.visibility = Visibility(int(value))
            variant.save()
    items = (
        DocumentVariant.objects
        .annotate(content_len=Length('content'))
        .filter(content_len__lte=2300)
        .filter(visibility=Visibility.UNREVIEWED)
        .filter(content_type__mime_type='text/html')
        .values('id', 'content_len')
    )
    return render(request, 'viewer/check_small.html', {'items': items})
